var APlus = angular.module('APlus', ['ui.router']);

APlus.config(function($stateProvider, $urlRouterProvider){

	$stateProvider

	.state('main',{
		url: '/main',
		templateUrl: '../views/login.html'

	});

	$urlRouterProvider.otherwise('/main');

});